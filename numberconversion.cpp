#include "numberconversion.h"
using namespace std;

int temp_Conversion(char numeral){
  if(numeral=='M' or numeral=='m')
    return 1000;
  else if(numeral=='D' or numeral=='d')
    return 500;
  else if(numeral=='C' or numeral=='c')
    return 100;
  else if(numeral=='L' or numeral=='l')
    return 50;
  else if(numeral=='X' or numeral=='x')
    return 10;
  else if(numeral=='V' or numeral=='v')
    return 5;
  else if(numeral=='I' or numeral=='i')
    return 1;
  else
    return 0;
}

int romantoint(std::string roman_numeral){
  int val = 0, temp = 0;

  for(int i=0; i<(roman_numeral.size()); i++){
    temp = temp_Conversion(roman_numeral[i]);

    if(temp_Conversion(roman_numeral[i+1]) > temp)
      val -= temp;
    else
      val += temp;
  }
  return val;
}

std::string inttoroman(int number) {

  string roman_numerals[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
  int int_values[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

  string result = "";

  for (int i = 0; i < 13; ++i){
    while(number - int_values[i] >= 0){
      result += roman_numerals[i];
      number -= int_values[i];
    }
  }
  return result;
}
